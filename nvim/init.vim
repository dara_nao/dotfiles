
source $HOME/.config/nvim/themes/airline.vim 

call plug#begin('~/.vim/plugged')
        Plug 'junegunn/vim-easy-align'
        Plug 'vim-airline/vim-airline'
        Plug 'vim-airline/vim-airline-themes'
        Plug 'justinmk/vim-sneak'
	Plug 'xuhdev/vim-latex-live-preview', { 'for': 'tex' }
	Plug 'lervag/vimtex'
	Plug 'mattn/emmet-vim'
call plug#end() 

"rMarkdown autocompile to PDF.
map <F2> :! pandoc % -o %:p:r.pdf && zathura %:p:r.pdf<CR>
map <F3> :! pdflatex % %:r.pdf %% zathura %:p:r.pdf<CR>
